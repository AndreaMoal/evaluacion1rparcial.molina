package com.example.evaluacion1rparciamolina;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        texto =(TextView)findViewById(R.id.recibirtxt);
        Bundle bundle = this.getIntent().getExtras();

        double resultado;
        double toneladas;
        toneladas = 0.0004536;
        double libras;
        libras = Integer.parseInt(bundle.getString("dato"));
        resultado=Double.valueOf((libras*toneladas));




        texto.setText(resultado + ""+"toneladas");

        texto.setText(String.format("%.6f", resultado));
        Log.e("operacion","onCreate");

    }
}